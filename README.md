# passcrypt

[![Build Status](https://travis-ci.org/m12n/passcrypt.svg?branch=master)](https://travis-ci.org/m12n/passcrypt) [![Coverage Status](https://coveralls.io/repos/github/m12n/passcrypt/badge.svg?branch=master)](https://coveralls.io/github/m12n/passcrypt?branch=master)

Simple library to read encrypted properties, such as passwords.

It uses strong AES-256 encryption, but encrypted passwords can be decrypted if the master key is known.

The master key is stored on the same machine as the encrypted password. That means, a user having access to a machine will be able to decrypt any password encrypted on the same machine.

The purpose of this encryption library is to provide a simple tool to avoid that passwords are stored in plaintext anywhere, and if it accidentially leaks into a VCS, it will be impossible to decrypt the password on a different machine.

The master key is always stored in a different location than the encrypted password.

## Installation

Add the following dependency:

```
<dependency>
    <groupId>org.m12n</groupId>
    <artifactId>passcrypt</artifactId>
    <version>0.0.7-SNAPSHOT</version>
</dependency>
```

Add the following repository:

```
<repository>
    <id>m12n-maven-repo-releases</id>
    <url>http://maven.m12n.org/releases</url>
</repository>
```

## Usage

### Encryption

To encrypt a password, you need to run the `org.m12n.passcrypt.cli.Main` class, which will prompt for the plaintext.

```
> java -jar passcrypt-0.0.7-SNAPSHOT.jar
Please enter plaintext:
```

The output will be something like

```
crypt:t0LuC7x871RO0NomJCdkxn8Xb/UATPDVEzzbQk9W9pc=
```

### Decryption

You can easily use the encrypted text in a property file, e.g.

```
database.password = crypt:t0LuC7x871RO0NomJCdkxn8Xb/UATPDVEzzbQk9W9pc=
```

To decrypt the password, you have to use

```
String encrypted = props.get("database.password");
String decrypted = PassCrypt.decrypt(encrypted);
```

### Fallback

The library detects an encrypted password by the `crypt:` prefix. If the prefix is missing, the text will not be decrypted and passed directly. This allows users that are not using the PassCrypt library to just use a plaintext password (not recommended, though).

## Limits

`Passcrypt` generates a unique master key on each machine. That means that an encrypted password is not portable to another machine.
