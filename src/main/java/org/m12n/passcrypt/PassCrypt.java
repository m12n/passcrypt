package org.m12n.passcrypt;

import org.m12n.passcrypt.crypto.CryptoService;
import org.m12n.passcrypt.key.MasterKey;
import org.m12n.passcrypt.key.MasterKeyService;

/**
 * Used to decrypt encrypted values.
 */
public class PassCrypt {
    private static final MasterKey key = new MasterKeyService().getMasterKey();
    private static final CryptoService cryptoService = new CryptoService();

    public static String decrypt(final String encrypted) {
        return cryptoService.decrypt(key, encrypted);
    }
}
