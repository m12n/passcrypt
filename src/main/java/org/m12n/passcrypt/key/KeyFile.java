package org.m12n.passcrypt.key;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

import static java.lang.System.getProperty;
import static java.nio.file.Files.exists;
import static java.nio.file.Files.getFileAttributeView;
import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Paths.get;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.attribute.PosixFilePermissions.fromString;

/**
 * Maintains the file of the encryption key and the file permissions.
 */
class KeyFile {
    static final Path KEY_FILE_PATH = get(getProperty("user.home"), ".passcrypt", "keyfile");
    static final String FILE_PERMISSIONS_STR = "r--------";
    private static final Set<PosixFilePermission> FILE_PERMISSIONS = fromString(FILE_PERMISSIONS_STR);

    public MasterKey read() throws IOException {
        if (exists(KEY_FILE_PATH)) {
            final PosixFileAttributeView view = getView();
            final Set<PosixFilePermission> permissions = view.readAttributes().permissions();
            final String permissionsStr = PosixFilePermissions.toString(permissions);
            if (!permissionsStr.equals(FILE_PERMISSIONS_STR)) {
                System.err.printf("Invalid keyfile permissions, expected %s but was %s%n", FILE_PERMISSIONS_STR, permissionsStr);
                setPermissions();
            }
            return new MasterKey(readAllBytes(KEY_FILE_PATH));
        }
        else {
            return null;
        }
    }

    public void write(final MasterKey key) throws IOException {
        KEY_FILE_PATH.getParent().toFile().mkdirs();
        Files.write(KEY_FILE_PATH, key.getBytes(), CREATE);
        setPermissions();
    }

    private void setPermissions() throws IOException {
        getView().setPermissions(FILE_PERMISSIONS);
    }

    private PosixFileAttributeView getView() {
        return getFileAttributeView(KEY_FILE_PATH, PosixFileAttributeView.class);
    }
}
