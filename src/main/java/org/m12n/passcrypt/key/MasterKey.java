package org.m12n.passcrypt.key;

import org.m12n.passcrypt.util.RandomData;

/**
 * Represents the master key of the de-/encryption process. The key is always 256 bit long.
 * It will be generated only once and stored in the {@link KeyFile}.
 */
public class MasterKey extends RandomData {
    public static final int SIZE = 256 / 8;

    public MasterKey() {
        super(SIZE);
    }

    public MasterKey(final byte[] bytes) {
        super(SIZE, bytes);
    }
}
