package org.m12n.passcrypt.key;

import java.io.IOException;

/**
 * Service used to retrieve the {@link MasterKey}.
 */
public class MasterKeyService {
    public MasterKey getMasterKey() {
        final KeyFile file = new KeyFile();
        MasterKey key = null;
        try {
            key = file.read();
        }
        catch (final IOException e) {
            // ignore
        }
        if (key == null) {
            key = new MasterKey();
            try {
                file.write(key);
            }
            catch (final IOException e) {
                throw new IllegalStateException("Unable to create master key", e);
            }
        }
        return key;
    }
}
