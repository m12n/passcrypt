package org.m12n.passcrypt.crypto;

import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.m12n.passcrypt.key.MasterKey;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * Performs the actual encryption.
 * Uses AES-256.
 * Encrypted output is a Base-64 encoded data. The first 128 bit are the IV, the
 * remaining data is the actual encrypted text.
 * The encrypted output is prefixed by {@code crypt:}.
 */
public class CryptoService {

    public static final boolean MODE_ENCRYPT = true;
    public static final boolean MODE_DECRYPT = false;
    public static final String PREFIX = "crypt:";

    public String encrypt(final MasterKey key, final String plaintext) {
        final InitializationVector iv = new InitializationVector();

        final byte[] processed = processCipher(key, iv, plaintext.getBytes(StandardCharsets.UTF_8), MODE_ENCRYPT);

        final EncryptionData data = new EncryptionData(iv.getBytes(), processed);

        return PREFIX + Base64.getEncoder().encodeToString(data.getMerged());
    }

    public String decrypt(final MasterKey key, final String encryptedText) {
        if (encryptedText == null || !encryptedText.startsWith(PREFIX)) {
            return encryptedText;
        }
        final byte[] merged = Base64.getDecoder().decode(encryptedText.substring(PREFIX.length()));

        final EncryptionData data = new EncryptionData(merged, InitializationVector.SIZE);
        final InitializationVector iv = new InitializationVector(data.getIv());

        final byte[] decrypted = processCipher(key, iv, data.getEncrypted(), MODE_DECRYPT);

        return new String(decrypted, StandardCharsets.UTF_8);
    }

    private byte[] processCipher(final MasterKey key, final InitializationVector iv, final byte[] data, final boolean mode) {
        try {
            final PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CBCBlockCipher(new AESFastEngine()));
            cipher.init(mode, new ParametersWithIV(new KeyParameter(key.getBytes()), iv.getBytes()));
            final byte[] buffer = new byte[cipher.getOutputSize(data.length)];
            final int len1 = cipher.processBytes(data, 0, data.length, buffer, 0);
            final int len2 = cipher.doFinal(buffer, len1);
            final byte[] result = new byte[len1 + len2];
            System.arraycopy(buffer, 0, result, 0, result.length);
            return result;
        }
        catch (final InvalidCipherTextException e) {
            throw new IllegalStateException(e);
        }
    }

}
