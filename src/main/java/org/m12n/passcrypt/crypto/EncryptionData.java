package org.m12n.passcrypt.crypto;

import static java.lang.System.arraycopy;

class EncryptionData {
    private final byte[] merged;
    private final byte[] iv;
    private final byte[] encrypted;

    public EncryptionData(final byte[] merged, final int split) {
        this.merged = merged;
        iv = new byte[split];
        encrypted = new byte[merged.length - iv.length];
        arraycopy(merged, 0, iv, 0, iv.length);
        arraycopy(merged, iv.length, encrypted, 0, encrypted.length);
    }

    public EncryptionData(final byte[] iv, final byte[] encrypted) {
        this.iv = iv;
        this.encrypted = encrypted;
        merged = new byte[iv.length + encrypted.length];
        arraycopy(iv, 0, merged, 0, iv.length);
        arraycopy(encrypted, 0, merged, iv.length, encrypted.length);
    }

    public byte[] getEncrypted() {
        return encrypted;
    }

    public byte[] getIv() {
        return iv;
    }

    public byte[] getMerged() {
        return merged;
    }
}
