package org.m12n.passcrypt.crypto;

import org.m12n.passcrypt.util.RandomData;

class InitializationVector extends RandomData {
    public static final int SIZE = 128 / 8;

    public InitializationVector() {
        super(SIZE);
    }

    public InitializationVector(final byte[] bytes) {
        super(SIZE, bytes);
    }
}
