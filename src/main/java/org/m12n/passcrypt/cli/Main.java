package org.m12n.passcrypt.cli;

import org.m12n.passcrypt.crypto.CryptoService;
import org.m12n.passcrypt.key.MasterKey;
import org.m12n.passcrypt.key.MasterKeyService;

/**
 * CLI interface to encrypt plaintext. Prompts for the plaintext and prints the encrypted
 * String to the console.
 */
public class Main {
    public static void main(final String[] args) {
        final String plaintext = PasswordInput.getInstance().readPassword("Please enter plaintext: ");

        final MasterKey key = new MasterKeyService().getMasterKey();
        final String encrypted = new CryptoService().encrypt(key, plaintext);

        System.out.println(encrypted);
    }
}
