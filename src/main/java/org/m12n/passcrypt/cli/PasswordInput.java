package org.m12n.passcrypt.cli;

import java.io.Console;
import java.util.Scanner;

public abstract class PasswordInput {

    private static PasswordInput instance;

    private PasswordInput() {
    }

    public static PasswordInput getInstance() {
        return instance == null ? instance = newInstance() : instance;
    }

    private static PasswordInput newInstance() {
        final Console console = System.console();
        if (console != null) {
            return new ConsolePasswordInput(console);
        }
        return new StandardPasswordInput();
    }

    public abstract String readPassword(String fmt, Object... args);

    private static class ConsolePasswordInput extends PasswordInput {
        private final Console console;

        public ConsolePasswordInput(final Console console) {
            this.console = console;
        }

        @Override
        public String readPassword(final String fmt, final Object... args) {
            return String.valueOf(console.readPassword(fmt, args));
        }
    }

    private static class StandardPasswordInput extends PasswordInput {
        @Override
        public String readPassword(final String fmt, final Object... args) {
            System.out.printf(fmt, args);
            return new Scanner(System.in).nextLine();
        }
    }
}
