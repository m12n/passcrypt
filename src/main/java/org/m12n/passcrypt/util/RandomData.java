package org.m12n.passcrypt.util;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Objects;

public class RandomData {
    private final byte[] bytes;

    protected RandomData(final int size) {
        bytes = new byte[size];
        new SecureRandom().nextBytes(bytes);
    }

    protected RandomData(final int size, final byte[] bytes) {
        Objects.requireNonNull(bytes);
        if (size >= 0 && bytes.length != size) {
            throw new IllegalArgumentException("Data must be " + size + " long, but was " + bytes.length);
        }
        this.bytes = bytes;
    }

    public byte[] getBytes() {
        return bytes;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj != null) {
            if (obj == this) {
                return true;
            }
            if (obj.getClass().equals(getClass())) {
                final RandomData data = (RandomData) obj;
                return Arrays.equals(bytes, data.bytes);
            }
        }
        return false;
    }
}
