package org.m12n.passcrypt.util;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class RandomDataTest {
    @Test
    public void verifyEquals() {
        final RandomData randomData = new RandomData(8);
        assertThat(randomData, is(not(equalTo(null))));
        assertThat(randomData, is(equalTo(randomData)));
    }
}
