package org.m12n.passcrypt.cli;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class MainTest {
    @Test
    public void verifyMain() {
        System.setIn(new ByteArrayInputStream("abc".getBytes()));
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        Main.main(new String[0]);
        assertThat(out.toString(), is(not("")));
    }
}
