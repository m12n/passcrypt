package org.m12n.passcrypt.key;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

public abstract class AbstractKeyFileTest {
    public static final Path PARENT = KeyFile.KEY_FILE_PATH.getParent();
    private static PrintStream origErr;

    @BeforeClass
    public static void backup() throws IOException {
        origErr = System.err;
        try {
            Files.move(PARENT, Paths.get(PARENT.toString() + ".bak"));
        }
        catch (final NoSuchFileException e) {
            // ingore
        }
    }

    @AfterClass
    public static void restore() throws IOException {
        System.setErr(origErr);
        try {
            Files.move(Paths.get(PARENT.toString() + ".bak"), PARENT);
        }
        catch (final NoSuchFileException e) {
            // ignore
        }
    }

    @Before
    @After
    public void deleteKeyFile() throws IOException {
        final Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rw-------");
        final PosixFileAttributeView view = Files.getFileAttributeView(KeyFile.KEY_FILE_PATH, PosixFileAttributeView.class);
        try {
            view.setPermissions(perms);
            Files.delete(KeyFile.KEY_FILE_PATH);
            Files.delete(KeyFile.KEY_FILE_PATH.getParent());
        }
        catch (final NoSuchFileException e) {
            // ignore
        }
    }
}
