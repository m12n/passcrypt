package org.m12n.passcrypt.key;

import org.junit.Assert;
import org.junit.Test;

/**
 */
public class MasterKeyServiceTest extends AbstractKeyFileTest {
    @Test
    public void verifyGetMasterKey() {
        final MasterKeyService service = new MasterKeyService();
        final MasterKey k1 = service.getMasterKey();
        final MasterKey k2 = service.getMasterKey();

        Assert.assertEquals(k1, k2);
    }
}
