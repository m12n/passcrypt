package org.m12n.passcrypt.key;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class KeyFileTest extends AbstractKeyFileTest {
    @Test
    public void verifyRead() throws Exception {
        final KeyFile keyFile = new KeyFile();

        assertNull(keyFile.read());

        final MasterKey data = new MasterKey();

        keyFile.write(data);

        assertEquals(data, keyFile.read());
    }

    @Test
    public void verifyFilePermissions() throws IOException {
        final ByteArrayOutputStream err = new ByteArrayOutputStream();
        System.setErr(new PrintStream(err));
        
        final KeyFile keyFile = new KeyFile();
        final MasterKey masterKey = new MasterKey();
        keyFile.write(masterKey);

        assertEquals(KeyFile.FILE_PERMISSIONS_STR, getPermissions());

        getView().setPermissions(PosixFilePermissions.fromString("rw-rw-rw-"));

        assertEquals(masterKey, keyFile.read());

        assertEquals(KeyFile.FILE_PERMISSIONS_STR, getPermissions());

        assertEquals("Invalid keyfile permissions, expected r-------- but was rw-rw-rw-\n", err.toString());
    }

    private String getPermissions() throws IOException {
        final PosixFileAttributeView view = getView();
        final PosixFileAttributes attributes = view.readAttributes();
        final Set<PosixFilePermission> permissions = attributes.permissions();
        return PosixFilePermissions.toString(permissions);
    }

    private PosixFileAttributeView getView() {
        return Files.getFileAttributeView(KeyFile.KEY_FILE_PATH, PosixFileAttributeView.class);
    }
}
