package org.m12n.passcrypt.key;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 */
public class MasterKeyTest {
    @Test(expected = IllegalArgumentException.class)
    public void verifyWrongDataSize() throws Exception {
        new MasterKey("123".getBytes());
    }

    @Test(expected = NullPointerException.class)
    public void verifyNullData() {
        new MasterKey(null);
    }

    @Test
    public void verifyGetBytes() {
        final byte[] bytes = new byte[256 / 8];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) i;
        }
        final MasterKey key = new MasterKey(bytes);
        assertArrayEquals(bytes, key.getBytes());
    }

    @Test
    public void verifyUniqueBytes() {
        assertFalse(Arrays.equals(new MasterKey().getBytes(), new MasterKey().getBytes()));
    }

    @Test
    public void verifyBytesLength() {
        assertEquals(256 / 8, new MasterKey().getBytes().length);
    }
}


