package org.m12n.passcrypt.crypto;

import org.junit.Test;
import org.m12n.passcrypt.key.MasterKey;

import static org.junit.Assert.assertEquals;

/**
 */
public class CryptoServiceTest {

    @Test
    public void testEncryptionAndDecryption() {
        final String plaintext = "Hello, World! Hello, World! Hello, World! Hello, World! Hello, World! Hello, World! Hello, World! Hello, World! Hello, World! Hello, World! Hello, World!";
        final MasterKey key = new MasterKey();

        final CryptoService service = new CryptoService();
        final String encrypt = service.encrypt(key, plaintext);

        final String decrypt = service.decrypt(key, encrypt);

        assertEquals(plaintext, decrypt);
    }

    @Test
    public void testUnencryptedInput() {
        final MasterKey key = new MasterKey();
        final CryptoService service = new CryptoService();

        final String str = "Hello, World!";
        assertEquals(str, service.decrypt(key, str));
    }
}
