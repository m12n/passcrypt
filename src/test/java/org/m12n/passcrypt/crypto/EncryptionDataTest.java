package org.m12n.passcrypt.crypto;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

/**
 */
public class EncryptionDataTest {
    @Test
    public void verifyEncryptionData() {
        final byte[] a = createBytes(2), b = createBytes(4);

        final EncryptionData e1 = new EncryptionData(a, b);
        final EncryptionData e2 = new EncryptionData(e1.getMerged(), 2);

        assertArrayEquals(a, e1.getIv());
        assertArrayEquals(b, e1.getEncrypted());
    }

    private byte[] createBytes(final int length) {
        final byte[] bytes = new byte[length];
        for (byte i = 0; i < bytes.length; i++) {
            bytes[i] = i;
        }
        return bytes;
    }
}
