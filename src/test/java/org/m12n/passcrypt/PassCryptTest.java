package org.m12n.passcrypt;

import org.junit.Assert;
import org.junit.Test;
import org.m12n.passcrypt.crypto.CryptoService;
import org.m12n.passcrypt.key.MasterKey;
import org.m12n.passcrypt.key.MasterKeyService;

/**
 */
public class PassCryptTest {
    @Test
    public void verifyDecrypt() {
        final MasterKey masterKey = new MasterKeyService().getMasterKey();
        final String plaintext = "1234";
        final String encrypt = new CryptoService().encrypt(masterKey, plaintext);

        Assert.assertEquals(plaintext, PassCrypt.decrypt(encrypt));
    }
}
